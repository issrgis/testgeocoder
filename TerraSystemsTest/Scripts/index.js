﻿var geocodeUrl = "https://gis.azgeo.az.gov/arcgis/rest/services/Locators/USA_StreetAddress_2017_R1/GeocodeServer/geocodeAddresses";
var agsrToken = "xzm7tobRqQb5xA4wTA_DE1XQqr4udlUEg3C6wChOzA8oTrpb-PGDw2YAfgcanX0x";


var addressesCollection = {
    "records": [
        {
            "attributes": {
                "OBJECTID": 1,
                "Street": "1745 E Southern Ave",
                "City": "Tempe",
                "State": "AZ",
                "ZIP": "85282",
            }
        },
        {
            "attributes": {
                "OBJECTID": 2,
                "Street": "1825 E Warner Rd",
                "City": "Tempe",
                "State": "AZ",
                "ZIP": "85284",
            }
        }
    ]
};

var payload = {
    f: 'json',
    token: agsrToken,
    addresses: JSON.stringify(addressesCollection)
}

$.noConflict();

jQuery(document).ready(function () {
    jQuery.ajax({
        url: geocodeUrl + agsrToken,
        method: "POST",
        dataType: "jsonp",
        jsonpCallback: "ProcessResults",
        data: payload
    })
        .done(function (geocodeResponse) {
            console.log('success', geocodeResponse)
            jQuery('.xhrRawOutput').html(JSON.stringify(geocodeResponse));
        })
        .fail(function (xhr) {
            console.log('error', xhr);
            jQuery('.xhrErrorOutput').html(JSON.stringify(xhr));
        });
});



